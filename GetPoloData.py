from Constants import PERIODS

from datetime import date, datetime, timedelta
from time import mktime

from poloniex import Poloniex
from coinmarketcap import Market

from Logger import get_logger

logger = get_logger()


def getCoinData(ticker_, period_, start_date, end_date):
    polo = Poloniex()
    ticker = 'BTC_{}'.format(ticker_)
    try:
        data = polo.returnChartData(
            ticker, period=period_, start=start_date, end=end_date)
        return data
    except Exception as e:
        logger.error('Data not found for {}, {}'.format(ticker, e))
        return


def getTimePeriod(days=300):
    today = date.today()
    start_date = today - timedelta(days)
    start = mktime(start_date.timetuple())
    end = mktime(today.timetuple())
    return start, end


def main():
    start_, end_ = getTimePeriod()
    tick = getCoinData('ETH', PERIODS[
                       'DAY'], start_date=start_, end_date=end_)
    print(tick[0])
    print(tick[-1])

if __name__ == '__main__':
    main()
