from bs4 import BeautifulSoup
from Constants import EXCHANGE_PAGES
from csv import DictWriter

from Logger import get_logger
import requests
import os

from coinmarketcap import Market

FIELDNAMES = ['symbol', 'id', 'name', 'rank', 'last_updated']
FOLDER = 'tickers/'

logger = get_logger()
# TODO: add comments to function parts.


def exchangeCoinsToCSV(page, folder):
    """
    Pulls data from CoinMCap.
    Parses data.
    Creates a list of coins on the exhange page.
    """
    exchange_name = page['name']
    webpage = page['page']

    coi = Market()
    response = requests.get(webpage)
    soup = BeautifulSoup(response.text, 'html.parser')

    coins = []
    href = soup.find_all('a', target='_blank')
    for x in href:
        line = x.text
        if '/BTC' in line:
            parsed = line.replace('/BTC', '')
            coins.append(parsed)

    if not os.path.exists(folder):
        os.makedirs(folder)
        logger.info('Creating {} folder'.format(folder))

    with open('{0}{1}.csv'.format(folder, exchange_name), 'w') as file:
        writer = DictWriter(file, fieldnames=FIELDNAMES)
        writer.writeheader()

        for ticker in coi.ticker():
            parsed_coins = {}
            if ticker['symbol'] in coins:
                for name in FIELDNAMES:
                    parsed_coins[name] = ticker[name]
                writer.writerow(parsed_coins)
                logger.info('Added {0} to {1}.csv'.format(
                    ticker['symbol'], exchange_name))


def getExchangeTickerInfo():
    """
    Cycles through xchange_pages, pulls data. 
    Creates Exchange.csv
    """
    for page in EXCHANGE_PAGES:
        exchangeCoinsToCSV(page, FOLDER)

if __name__ == '__main__':
    getExchangeTickerInfo()
