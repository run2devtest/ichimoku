
PERIODS = {
    'DAY': 86400,
    '4H': 14400,
    '2H': 7200,
    '30M': 1800,
    '15M': 900,
    '5M': 300}

FILTER_DEFAULTS = {
    'VOLUME_MIN': 1,
    'MIN_COIN_VALUE': 1,
    'MARKET_CAP_MIN': 1,
}

POLONIEX = {
    'page': 'https://coinmarketcap.com/exchanges/poloniex/', 'name': 'Poloniex'}
BITTREX = {
    'page': 'https://coinmarketcap.com/exchanges/bittrex/', 'name': 'Bittrex'}

EXCHANGE_PAGES = [POLONIEX, BITTREX]
