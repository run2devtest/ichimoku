
import requests


def returnChartData(ticker_, interval_, periods_=1):

    url = 'https://bittrex.com/Api/v2.0/pub/market/GetTicks?marketName={ticker}&tickInterval={interval}'.format(
        ticker=ticker_, interval=interval_)
    print(url)

    response = requests.get(url)
    print(response.json()['result'][:periods_])


returnChartData('BTC-STRAT', 'Day', 2)
