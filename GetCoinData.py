from Constants import PERIODS

from datetime import date, datetime, timedelta
from time import mktime

from poloniex import Poloniex
from coinmarketcap import Market

from Logger import get_logger

polo = Poloniex()
coincap = Market()
# # ticker = polo.returnTicker()
yesterday = date.today() - timedelta(1)
unix_secs = mktime(yesterday.timetuple())

ticker = polo.returnChartData('BTC_ETH', PERIODS['DAY'], start=unix_secs)
print(help(polo))
print(ticker)
print(coincap.ticker('Ethereum'))
print('\n\n')

# currencies = polo.returnCurrencies()
# for x in currencies:
#     name = currencies[x]['name']
#     print(currencies[x])
#     print(name)
#     try:
#         print(coincap.ticker(str(name)))
#     except:
#         pass
#     print('\n')


def get_coin_data(ticker, start_date, end_date):
    polo = Poloniex()
    try:
        data = polo.returnChartData(ticker, start=start_date, end=end_date)
    except:
        pass
